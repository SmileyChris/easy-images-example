from django import shortcuts, forms
from easy_images.image import annotate

from . import models


class ImageForm(forms.ModelForm):
    class Meta:
        fields = ['file']
        model = models.TestImage


def home(request):
    if request.method == 'POST':
        form = ImageForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
        return shortcuts.redirect(request.path)
    form = ImageForm()
    images = models.TestImage.objects.all()
    image_map = {
        'tiny': {'crop': (16, 16)},
        'thumb': {'crop': (300, 100), 'upscale': True},
    }
    annotate(images, image_map, 'file')
    return shortcuts.render(request, 'home.html', {
        'images': images, 'form': form})
