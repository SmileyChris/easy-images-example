import os

from django.db import models


class TestImage(models.Model):
    file = models.FileField()

    def __unicode__(self):
        return os.path.splitext(os.path.basename(self.file.name))[0]
